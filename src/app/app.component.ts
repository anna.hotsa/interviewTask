import {Component, OnInit} from '@angular/core';
import {HttpService} from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [HttpService]
})
export class AppComponent implements OnInit {

  public searchedShows: any = [];
  public searchName = '';
  public selectedShow: any = {};
  public areShowDetailsShowed: boolean = false;

  constructor(private httpService: HttpService) {
  }

  ngOnInit() {
  }

  public searchShows(name: string) {
    this.areShowDetailsShowed = false;

    this.httpService.getSearchedShows(name).subscribe(message => {
      console.log(message);
      this.searchedShows = message;
    });
  }

  public showDetails(id: number) {
    this.areShowDetailsShowed = true;

    const index = this.searchedShows.findIndex(el => {
      return el.show.id === id;
    });

    this.selectedShow = this.searchedShows[index];
  }
}
